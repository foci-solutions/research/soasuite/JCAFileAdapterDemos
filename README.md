# JCA File Adapter Demos

These demos help explore the JCA File Adapter and some of its hidden secrets.

To run the demo you need to have created some directories on a Unix file system. However these values are easy to change.

* mkdir -p /u01/fileExchange/incoming
* mkdir -p /u01/fileExchange/outgoing
* touch /u01/fileExchange/incoming/test.csv